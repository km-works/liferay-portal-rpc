/*
 *  Copyright (C) 2005-2014 Christian P. Lerch <christian.p.lerch [at] gmail.com>
 * 
 *  This library is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU Lesser General Public License as published by the Free
 *  Software Foundation; either version 3 of the License, or (at your option)
 *  any later version.
 * 
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more 
 *  details.
 * 
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this distribution. If not, see <http://www.gnu.org/licenses/>.
 */
package org.kmworks.liferay.rpc.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cpl
 */
public class InputStreamSerializationProxyTest {
  
  public InputStreamSerializationProxyTest() {
  }

  @Test
  public void testSerializationProxy() throws FileNotFoundException, IOException {
    String tempDir = System.getProperty("java.io.tmpdir");
    System.out.println("Temp-Dir:  " + tempDir);
    File tempFile = File.createTempFile("ABC", null);
    System.out.println("Temp-File: " + tempFile.getAbsolutePath());
    InputStreamSerializationProxy proxy = new InputStreamSerializationProxy(tempFile.getName());
    InputStream is = proxy;
    Class isc = is.getClass();
    Class issc = isc.getSuperclass();
    assertEquals(issc.getName(), "java.io.InputStream");
    assertEquals(isc.getName(), "org.kmworks.liferay.rpc.utils.InputStreamSerializationProxy");
    assertTrue(is instanceof java.io.InputStream);
    File backingFile = null;
    if (is instanceof InputStreamSerializationProxy) {
      InputStreamSerializationProxy pr = (InputStreamSerializationProxy) is;
      backingFile = new File(tempDir, pr.getBackingFileName());
      is = new FileInputStream(backingFile);
    }
    assertFalse(is == null);
    is.close();
    backingFile.delete();
  }

}
