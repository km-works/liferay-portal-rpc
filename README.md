Liferay Portal RPC
==================

A stand-alone Java [RPC](https://en.wikipedia.org/wiki/Remote_procedure_call) client/server subsystem for remote access to the [Liferay Portal](http://www.liferay.com/products/liferay-portal) 6.2.0+ local or remote [API](http://docs.liferay.com/portal/6.2/javadocs/).

When releases become available you can download binary artifacts from [here](https://bitbucket.org/km-works/liferay-portal-rpc/downloads).

For documentation please visit our [Wiki](https://bitbucket.org/km-works/liferay-portal-rpc/wiki/Home).

License: LGPL v3