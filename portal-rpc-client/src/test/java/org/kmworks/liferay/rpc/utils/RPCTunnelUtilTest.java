/*
 *  Copyright (C) 2005-2014 Christian P. Lerch <christian.p.lerch [at] gmail.com>
 * 
 *  This library is free software; you can redistribute it and/or modify it under
 *  the terms of the GNU Lesser General Public License as published by the Free
 *  Software Foundation; either version 3 of the License, or (at your option)
 *  any later version.
 * 
 *  This library is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more 
 *  details.
 * 
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with this distribution. If not, see <http://www.gnu.org/licenses/>.
 */
package org.kmworks.liferay.rpc.utils;

import com.google.common.hash.Hashing;
import com.google.common.io.Files;
import com.liferay.portal.security.auth.HttpPrincipal;
import com.liferay.portal.service.http.HttpTestUtil;
import java.io.File;
import java.io.InputStream;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cpl
 */
public class RPCTunnelUtilTest {
  
  public RPCTunnelUtilTest() {
  }

  @Test
  public void testUploadDownloadInputStream() throws Exception {
    
    HttpPrincipal principal = HttpTestUtil.HTTP_PRINCIPAL;
    
    String upStreamFileName = "/some.txt";
    String upStreamFilePath = getClass().getResource(upStreamFileName).toString().substring(6);
    String upStreamHash = Files.hash(new File(upStreamFilePath), Hashing.goodFastHash(128)).toString();
    
    InputStream upStream = getClass().getResourceAsStream(upStreamFileName);
    String serverFileName = RPCTunnelUtil.uploadInputStream(principal, upStream);
    
    File downStreamFile = RPCTunnelUtil.downloadInputStream(principal, serverFileName);
    String downStreamHash = Files.hash(downStreamFile, Hashing.goodFastHash(128)).toString();
    
    assertEquals(upStreamHash, downStreamHash);
  }
  
}
