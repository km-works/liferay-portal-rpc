/*
 * Copyright (C) 2005-2014 Christian P. Lerch <christian.p.lerch [at] gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.liferay.portal.service.http;

import com.liferay.portal.security.auth.HttpPrincipal;
import com.liferay.portal.util.PropsUtil;
import org.kmworks.liferay.rpc.utils.AuthToken;
import org.kmworks.liferay.rpc.utils.AuthTokenFactory;

/*
 * Set Maven option -DfailIfNoTests=false to ignore test results for building from sources
 * 
 * @author Christian P. Lerch
 */
public class HttpTestUtil {
  
  public static final String SHARED_SECRET = "1234567890987654";   // change this to match the config of your portal server
  
  public static final String PORTAL_URL = "http://localhost:8080"; // portal instance base URI:
                                                                    // you must provide the actual virtual hostname here
  public static final String USER_NAME = "admin";                  // test user login name; could also be e-mail, or user id
                                                                    // as configured in your basic authentication settings
  public static final String USER_PWD = "manager";                 // test user plain-text password

  public static final HttpPrincipal HTTP_PRINCIPAL = new HttpPrincipal(PORTAL_URL, USER_NAME, USER_PWD);
  
  static {
    PropsUtil.set("tunneling.servlet.shared.secret", SHARED_SECRET);
  }
  
  protected static AuthToken getToken() {
    return AuthTokenFactory.getToken(HTTP_PRINCIPAL);
  }
  
}
