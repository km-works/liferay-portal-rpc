/*
 * Copyright (C) 2005-2014 Christian P. Lerch <christian.p.lerch [at] gmail [dot] com>
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this distribution. If not, see <http://www.gnu.org/licenses/>.
 */
package org.kmworks.liferay.rpc.utils;

import com.liferay.portal.security.auth.HttpPrincipal;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Christian P. Lerch
 */
public final class AuthTokenFactory {
  
  private static final AuthTokenFactory INSTANCE = new AuthTokenFactory();
  
  private AuthTokenFactory() {}
  
  private final Map<HttpPrincipal, AuthToken> cache_ = new HashMap<>();
  
  protected Map<HttpPrincipal, AuthToken> getCache() {
    return cache_;
  }
  
  public static AuthToken getToken(HttpPrincipal principal) {
    Map<HttpPrincipal, AuthToken> cache = INSTANCE.getCache();
    AuthToken result = cache.get(principal);
    if (result == null) {
      result = new AuthToken(principal);
      cache.put(principal, result);
    }
    return result;
  }
  
  public static String principalAsString(HttpPrincipal principal) {
    final StringBuilder sb = new StringBuilder();
    sb.append(principal.getLogin()).append(":***@").append(principal.getUrl());
    return sb.toString();
  }
  
}
