/*
 * Copyright (C) 2005-2015 Christian P. Lerch <christian.p.lerch [at] gmail [dot] com>
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this distribution. If not, see <http://www.gnu.org/licenses/>.
 */
package org.kmworks.liferay.rpc.utils;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.io.ByteStreams;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.HttpHeaders;
import com.liferay.portal.kernel.servlet.HttpMethods;
import com.liferay.portal.kernel.util.Base64;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.MethodHandler;
import com.liferay.portal.kernel.util.ObjectValuePair;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.security.auth.HttpPrincipal;
import com.liferay.portal.security.auth.PrincipalException;
import com.liferay.portal.service.http.TunnelUtil;
import com.liferay.util.Encryptor;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Christian P. Lerch
 */
public class RPCTunnelUtil extends TunnelUtil {

	private static final Log _log = LogFactoryUtil.getLog(RPCTunnelUtil.class);
	private static final boolean _VERIFY_SSL_HOSTNAME = GetterUtil.getBoolean(
		PropsUtil.get(TunnelUtil.class.getName() + ".verify.ssl.hostname"));

	public static Object invoke(HttpPrincipal httpPrincipal, MethodHandler methodHandler)
    throws Exception {

    final HttpPrincipal principal = AuthToken.clonePrincipal(httpPrincipal);
    
    // Enable RPCTunnelServlet to check for matching shared secrects on client and server
		final String password = Encryptor.encrypt(getSharedSecretKey(), httpPrincipal.getLogin());
    principal.setPassword(password);
    
    // Prepare any input streams arguments for serialization by 
    //  first uploading their content to the server
    //  and then replace their values by (serializable) InputStreamSerializationProxy objects
    methodHandler = proxyInputStreamArguments(principal, methodHandler);
    
    // Prepare the ObjectValuePair object to be serialized
    final ObjectValuePair<HttpPrincipal, MethodHandler> ovp = new ObjectValuePair<>(principal, methodHandler);
    
    // Get the HTTP connection to the RPC tunnel servlet on the server-side
		final HttpURLConnection objStreamConn = getObjStreamConnection(principal);
    
    // Serialize and transmit the ObjectValuePair to the RPC tunnel servlet's request input stream
    try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(objStreamConn.getOutputStream())) {
      objectOutputStream.writeObject(ovp);
      objectOutputStream.flush();
    }
    
    // Prepare the return object, which represents the outcome of invoking the server-side method,
    //  with a dummy value
		Object returnObject = null;
    
    // Fetch and deserialize the return object from the RPC tunnel servlet's response output stream
		try (ObjectInputStream objectInputStream = new ObjectInputStream(objStreamConn.getInputStream())) {
        returnObject = objectInputStream.readObject();
		} catch (EOFException eofe) {
			if (_log.isDebugEnabled()) {
				_log.debug("Unable to read object from object stream", eofe);
			}
		} catch (IOException ioe) {
			String ioeMessage = ioe.getMessage();
			if (ioeMessage != null && ioeMessage.contains("HTTP response code: 401")) {
				throw new PrincipalException(ioeMessage);
			} else {
				throw ioe;
			}
		}

		if (returnObject != null) {
      if (returnObject instanceof Exception) {
        throw (Exception)returnObject;
      } else if (returnObject instanceof InputStreamSerializationProxy) {
        return unproxyInputStreamResult(principal, (InputStreamSerializationProxy)returnObject);
      }
		}
    
		return returnObject;
	}

  private static HttpURLConnection getObjStreamConnection(HttpPrincipal principal) 
          throws IOException {
    return _getConnection(new URL(principal.getUrl() + "/rpc-server-hook/api/do"), 
            principal.getLogin(), principal.getPassword(), 
            ContentTypes.APPLICATION_X_JAVA_SERIALIZED_OBJECT);
  }
  
  private static HttpURLConnection getUploadConnection(HttpPrincipal principal) 
          throws IOException {
    return _getConnection(new URL(principal.getUrl() + "/rpc-server-hook/api/upload"), 
            principal.getLogin(), principal.getPassword(), 
            ContentTypes.APPLICATION_OCTET_STREAM);
  }
  
  private static HttpURLConnection getDownloadConnection(HttpPrincipal principal) 
          throws IOException {
    return _getConnection(new URL(principal.getUrl() + "/rpc-server-hook/api/download"), 
            principal.getLogin(), principal.getPassword(), 
            ContentTypes.APPLICATION_OCTET_STREAM);
  }
  
	private static HttpURLConnection _getConnection(
          URL servletUrl, 
          String login, 
          String password, 
          String contentType)
		throws IOException {

    checkNotNull(servletUrl);
    checkNotNull(login);
    checkNotNull(password);
    checkNotNull(contentType);

		HttpURLConnection httpURLConnection = (HttpURLConnection)servletUrl.openConnection();
		httpURLConnection.setDoInput(true);
		httpURLConnection.setDoOutput(true);

		if (!_VERIFY_SSL_HOSTNAME && httpURLConnection instanceof HttpsURLConnection) {
			HttpsURLConnection httpsURLConnection = (HttpsURLConnection)httpURLConnection;
			httpsURLConnection.setHostnameVerifier(new HostnameVerifier() {

					@Override
					public boolean verify(String hostname, SSLSession session) {
						return true;
					}
      });
		}

		httpURLConnection.setRequestProperty(HttpHeaders.CONTENT_TYPE, contentType);
		httpURLConnection.setUseCaches(false);
		httpURLConnection.setRequestMethod(HttpMethods.POST);

    final String userNameAndPassword = login + StringPool.COLON + password;
    httpURLConnection.setRequestProperty(HttpHeaders.AUTHORIZATION,
      HttpServletRequest.BASIC_AUTH + StringPool.SPACE + Base64.encode(userNameAndPassword.getBytes()));

		return httpURLConnection;
	}

  private static MethodHandler proxyInputStreamArguments(HttpPrincipal principal, MethodHandler methodHandler) 
          throws IOException {
    
    Object[] args = methodHandler.getArguments();
    
    for (int i=0; i<args.length; i++) {
      if (args[i] instanceof InputStream) {
        // Upload input stream content to server temp file via RPC upload servlet
        //  close uploaded input stream
        //  return name of temporary file created on the server-side
        final String fileName = uploadInputStream(principal, (InputStream)args[i]);
        // Replace InputStream argument by InputStreamSerializationProxy
        args[i] = new InputStreamSerializationProxy(fileName);
      }
    }
    
    return new MethodHandler(methodHandler.getMethodKey(), args);
  }
  
  private static InputStream unproxyInputStreamResult(HttpPrincipal principal, InputStreamSerializationProxy proxy) 
          throws FileNotFoundException, IOException {
    final File localTempFile = downloadInputStream(principal, proxy.getBackingFileName());
    return new FileInputStream(localTempFile);
  }

  static String uploadInputStream(HttpPrincipal principal, InputStream is) 
          throws IOException {
    
    final HttpURLConnection uploadStreamConn = getUploadConnection(principal);
    
    final OutputStream os = uploadStreamConn.getOutputStream();
    ByteStreams.copy(is, os);   // uses only 4KByte memory for the copy buffer
    is.close();
    os.flush();
    
    final InputStream ris = uploadStreamConn.getInputStream();
    final byte[] buff = new byte[256];
    final int size = ris.read(buff);
    return new String(buff, 0, size);   // server-side file name
  }

  static File downloadInputStream(HttpPrincipal principal, String serverFileName) 
          throws IOException {
    
    final HttpURLConnection downloadStreamConn = getDownloadConnection(principal);
    
    final OutputStream os = downloadStreamConn.getOutputStream();
    os.write(serverFileName.getBytes());
    os.flush();
    
    long bytesCopied;
    final File localTempFile;
    try (InputStream ris = downloadStreamConn.getInputStream()) {
      localTempFile = File.createTempFile("RPC_", null);
      try (FileOutputStream ros = new FileOutputStream(localTempFile)) {
        bytesCopied = ByteStreams.copy(ris, ros);
      }
    }
    
    return localTempFile;
  }

}
