/*
 * Copyright (C) 2005-2014 Christian P. Lerch <christian.p.lerch [at] gmail [dot] com>
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more 
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this distribution. If not, see <http://www.gnu.org/licenses/>.
 */
package org.kmworks.liferay.rpc.utils;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Company;
import com.liferay.portal.model.User;
import com.liferay.portal.security.auth.Authenticator;
import com.liferay.portal.security.auth.HttpPrincipal;
import com.liferay.portal.service.http.CompanyServiceHttp;
import com.liferay.portal.service.http.UserServiceHttp;
import java.net.URI;
import java.net.URISyntaxException;
import org.kmworks.liferay.rpc.client.RPCExtensionsHttp;

/** Represents the result of authenticating a user's credentials against the Liferay user defintions.
 *
 * @author Christian P. Lerch
 */
public class AuthToken {
  
  private final HttpPrincipal principal;
  private final String plainPassword;
  
  private User user = null;
  private Long repositoryId = null;
  private Exception err = null;
  private long authenticationResult = Authenticator.FAILURE;
  
  public AuthToken(HttpPrincipal principal) {
    this.principal = principal;
    this.plainPassword = principal.getPassword();
    getTokenData();
  }
  
  public boolean isValid() {
    return authenticationResult == user.getUserId() && err == null;
  }
  
  public String getVirtualHost() {
    try {
      return new URI(principal.getUrl()).getHost();
    } catch (URISyntaxException ex) {
      err = ex;
      return null;
    }
  }
  
  public HttpPrincipal getPrincipal() {
    return principal;
  }
  
  public long getUserId() {
    return user.getUserId();
  }
  
  public long getRepositoryId() throws PortalException, SystemException {
    if (repositoryId == null) {
      repositoryId = RPCExtensionsHttp.getRepositoryId(this);
    }
    return repositoryId;
  }
  
  public Throwable getError() {
    return err;
  }
  
  private void getTokenData() {
    try {
      String virtualHost = getVirtualHost();
      if (virtualHost == null) {
        throw new PortalException("Cannot parse virtual host name from malformed portal URI: " + err.getMessage());
      }

      Company company = CompanyServiceHttp.getCompanyByVirtualHost(principal, virtualHost);
      if (company == null) {
        throw new PortalException("Cannot find company record for virtual host: " + virtualHost);
      }
      long companyId = company.getCompanyId();
      principal.setCompanyId(companyId);
      
      user = UserServiceHttp.getUserByScreenName(principal, principal.getCompanyId(), principal.getLogin());
      if (user == null) {
        throw new PortalException("Cannot find user record for: " + principal.getLogin());
      }
      
      authenticationResult = RPCExtensionsHttp.authenticateUser(clone(principal, plainPassword));
      if (authenticationResult != user.getUserId()) {
        throw new PortalException("Cannot authenticate user: " + principal.getLogin());
      }
      
    } catch (PortalException | SystemException ex) {
      err = ex;
    }
  }
  
  private HttpPrincipal clone(HttpPrincipal principal, String plainPassword) {
    HttpPrincipal result = clonePrincipal(principal);
    result.setPassword(plainPassword);
    return result;
  }
  
  public static HttpPrincipal clonePrincipal(HttpPrincipal principal) {
    HttpPrincipal result =  new HttpPrincipal(principal.getUrl(), principal.getLogin(), principal.getPassword());
    result.setCompanyId(principal.getCompanyId());
    return result;
  }
  
}
